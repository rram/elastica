// Sriramajayam

#ifndef EULER_ELASTICA_STRUCTS_H
#define EULER_ELASTICA_STRUCTS_H

#include <euler_ScalarMap.h>
#include <utility>
#include <cassert>

namespace euler
{
  // Configuration consisting of a sensitivity and state variable
  using SensitivityField = ScalarMap;
  using StateField = ScalarMap;
  using Sensitivity_and_State = std::pair<const SensitivityField*, const StateField*>;

  //! Helper struct for algorithmic parameters
  struct SolveParams
  {
    double EPS; //!< Absolute tolerance
    double resScale; //!< Scaling factor for convergence of residuals
    double dofScale; //!< Scaling factor for convergence of dofs
    int nMaxIters; //!< Max number of iterations
    bool verbose; //!< Print convergence details
    
    //! Sanity check on parameters
    inline void Check() const
    { assert(EPS>0. && resScale>0. && dofScale>0. && nMaxIters>0); }
  };

}

#endif
