// Sriramajayam

#ifndef EULER_UTILS_H
#define EULER_UTILS_H

#include <vector>

// Forward declarations
class Element;
class LocalToGlobalMap;

namespace euler
{
  class ScalarMap;
  
  //! Computes the nodal Cartesian coordinates given the angle theta
  //! Assumes that elements are numbered sequentially. 
  void AngleToCartesianMap(const ScalarMap& theta,
			   const std::vector<Element*>& ElmArray,
			   const LocalToGlobalMap& L2GMap,
			   std::vector<double>& xy);
}


#endif
