// Sriramajayam

#ifndef EULER_SCALAR_MAP_H
#define EULER_SCALAR_MAP_H

#include <StressWorkUtils.h>

namespace euler
{
  //! Encapsulate the state
  class ScalarMap: public VectorMap
  {
  public:
    //! \param[in] nnodes Number of nodes
    //! \param[in] spd Spatial dimension
    inline ScalarMap(const int nnodes)
      :VectorMap(nnodes, 1) {}
  
    //! Destructor, does nothing
    inline virtual ~ScalarMap() {}
  
    //! Copy constructor
    inline ScalarMap(const ScalarMap& Obj)
      :VectorMap(Obj) {}
      
    //! Disable assignment
    ScalarMap& operator=(const ScalarMap&) = delete;
  };


  //! Access by an operation to ScalarMap
  class ScalarMapAccess: public VectorMapAccess
  {
  public:
    inline ScalarMapAccess(const std::vector<int> n)
      :VectorMapAccess(n) {}
    inline ScalarMapAccess(const ScalarMapAccess& obj)
      :VectorMapAccess(obj) {}
    inline ~ScalarMapAccess() {}
    inline ScalarMapAccess& operator=(const ScalarMapAccess& rhs)
      {
	VectorMapAccess::operator=(rhs);
	return *this;
      }
  };

}

#endif
