// Sriramajayam

#include <euler_DirichletProblem.h>

namespace euler
{
  // Constructor
  DirichletProblem::DirichletProblem(const std::vector<double>& coords,
				     const std::vector<int>& conn,
				     const double ei,
				     const std::pair<int,std::array<double,2>>& datuum)
    :Elastica_Basic(coords, conn, ei),
     origin(datuum)
  {
    // State sovler
    std::vector<int> nnz;
    state_Asm->CountNonzeros(nnz);
    theta_solver.Initialize(nnz,
			    DirichletProblem_Residual_Func,
			    DirichletProblem_Jacobian_Func);
    
  }
    
  // Destructor
  DirichletProblem::~DirichletProblem()
  { std::cout<<"\nI am here "<<std::flush;
    PetscBool flag;
    auto ierr = PetscFinalized(&flag); CHKERRV(ierr);
    assert(flag==PETSC_TRUE && "1. PETSc finalized early. How come?");
    theta_solver.Destroy();
    std::cout<<"\nI should be here "<<std::flush;
  }


  // Main functionality
  void DirichletProblem::ComputeState(const std::map<int,double>& dirichlet_bcs,
				      const SolveParams& solve_params)
  {
    // Set the context
    ctx.state_Asm = state_Asm;
    ctx.theta = theta;
    ctx.tempVec = &theta_solver.tempVec;
    ctx.dirichlet_bcs = &dirichlet_bcs;
    ctx.Check();

    if(solve_params.verbose)
      std::cout<<"\neuler::DirichletProblem::Computing state..."<<std::flush;
    
    // Set the current theta as the initial guess
    const double* init_guess = &(theta->Get())[0];

    // Solve
    theta_solver.Solve(init_guess, &ctx, solve_params.verbose);

    // Get the solution
    double* dofValues;
    PetscErrorCode ierr = VecGetArray(theta_solver.solVec, &dofValues); CHKERRV(ierr);
    const int nDofs = L2GMap->GetTotalNumDof();
    for(int i=0; i<nDofs; ++i)
      theta->Set(i, &dofValues[i]);
    theta->SetInitialized();
    ierr = VecRestoreArray(theta_solver.solVec, &dofValues); CHKERRV(ierr);

    // Mark state as not dirty
    state_is_dirty = false;
    
    // Compute nodal cartesian coordinates
    ComputeCartesianCoordinates(origin.first, &origin.second[0]);
    
    // -- done --
    return;
  }

  
  
  // Consistency test for state calculations
  void DirichletProblem::StateConsistencyTest(const double* stateVals,
					      const std::map<int,double>& dirichlet_bcs)

  {
    // Perturbation values
    const double pEPS = 1.e-6;
    const double mEPS = -1.e-6;
    
    // Access the solver internals
    auto& snes = theta_solver.snes;
    auto& solVec = theta_solver.solVec;
    auto& resVec = theta_solver.resVec;
    auto& kMat = theta_solver.kMat;

    // Set values for the state
    const int nDof = L2GMap->GetTotalNumDof();
    PetscErrorCode ierr;
    for(int i=0; i<nDof; ++i)
      {
	ierr = VecSetValues(solVec, 1, &i, &stateVals[i], INSERT_VALUES); CHKERRV(ierr);
	theta->Set(i, &stateVals[i]);
      }
    ierr = VecAssemblyBegin(solVec); CHKERRV(ierr);
    ierr = VecAssemblyEnd(solVec); CHKERRV(ierr);
    theta->SetInitialized();
    
    // Dummy solve parameters with consistent linearization on
    SolveParams solve_params({.EPS=1.e-10, .resScale=1., .dofScale=1., .nMaxIters=10, .verbose=true});

    // Set the context
    ctx.state_Asm = state_Asm;
    ctx.theta = theta;
    ctx.tempVec = &theta_solver.tempVec;
    ctx.dirichlet_bcs = &dirichlet_bcs;
    ctx.Check();
    ierr = SNESSetApplicationContext(snes, &ctx); CHKERRV(ierr);

    // Assemble the stiffness matrix
    ierr = DirichletProblem_Jacobian_Func(snes, solVec, kMat, kMat, nullptr); CHKERRV(ierr);
    
    // Compute the residual at a pair of perturbed states
    Vec resPlus;
    ierr = VecDuplicate(resVec, &resPlus); CHKERRV(ierr);
    Vec resMinus;
    ierr = VecDuplicate(resVec, &resMinus); CHKERRV(ierr);
    Vec solPlus;
    ierr = VecDuplicate(solVec, &solPlus); CHKERRV(ierr);
    Vec solMinus;
    ierr = VecDuplicate(solVec, &solMinus); CHKERRV(ierr);

    // Perturb and compute residuals
    // Don't alter dirichlet dofs
    for(int a=0; a<nDof; ++a)
      if(dirichlet_bcs.find(a)==dirichlet_bcs.end())
	{
	  // Positive perturbation
	  ierr = VecCopy(solVec, solPlus); CHKERRV(ierr);
	  ierr = VecSetValues(solPlus, 1, &a, &pEPS, ADD_VALUES); CHKERRV(ierr);
	  ierr = VecAssemblyBegin(solPlus); CHKERRV(ierr);
	  ierr = VecAssemblyEnd(solPlus); CHKERRV(ierr);
	  for(int n=0; n<nDof; ++n)
	    { double val;
	      ierr = VecGetValues(solPlus, 1, &n, &val); CHKERRV(ierr);
	      theta->Set(n, &val); }
	  theta->SetInitialized();
	  
	  // Residual at positive perturbation
	  ierr = DirichletProblem_Residual_Func(snes, solPlus, resPlus, nullptr); CHKERRV(ierr);

	  // Negative perturbation
	  ierr = VecCopy(solVec, solMinus); CHKERRV(ierr);
	  ierr = VecSetValues(solMinus, 1, &a, &mEPS, ADD_VALUES); CHKERRV(ierr);
	  ierr = VecAssemblyBegin(solMinus); CHKERRV(ierr);
	  ierr = VecAssemblyEnd(solMinus); CHKERRV(ierr);
	  for(int n=0; n<nDof; ++n)
	    { double val;
	      ierr = VecGetValues(solMinus, 1, &n, &val); CHKERRV(ierr);
	      theta->Set(n, &val); }
	  theta->SetInitialized();

	  // Residual at negative perturbation
	  ierr = DirichletProblem_Residual_Func(snes, solMinus, resMinus, nullptr); CHKERRV(ierr);
	  
	  // Compare numerical and implemented stiffness
	  for(int b=0; b<nDof; ++b)
	    if(dirichlet_bcs.find(b)==dirichlet_bcs.end())
	      {
		double kval;
		ierr = MatGetValues(kMat, 1, &b, 1, &a, &kval); CHKERRV(ierr);
		double rplus, rminus;
		ierr = VecGetValues(resPlus, 1, &b, &rplus); CHKERRV(ierr);
		ierr = VecGetValues(resMinus, 1, &b, &rminus); CHKERRV(ierr);
		double knum = (rplus-rminus)/(pEPS-mEPS);
		//std::cout<<"\nK("<<a<<","<<b<<"): "<<kval<<" should be "<<knum<<std::flush;
		if(std::abs(knum-kval)>1.e-5)
		  {
		    //std::cout<<"\nrbc::TendonElastica::StateConsistency failed: "
		    //	 <<kval<<" should be close to "<<knum<<std::flush;
		    //std::cout<<"  <---- "<<std::flush;
		    assert(std::abs(knum-kval)<1.e-5);
		  }
	      }
	}

    // Clean up
    ierr = VecDestroy(&resPlus); CHKERRV(ierr);
    ierr = VecDestroy(&resMinus); CHKERRV(ierr);
    ierr = VecDestroy(&solMinus); CHKERRV(ierr);
    ierr = VecDestroy(&solPlus); CHKERRV(ierr);
    return;
  }
     
}
