// Sriramajayam

#ifndef EULER_ELASTICA_BASIC_H
#define EULER_ELASTICA_BASIC_H

#include <array>
#include <Element.h>
#include <LocalToGlobalMap.h>
#include <Assembler.h>
#include <euler_ElasticaStateOp.h>

namespace euler
{
  //! Helper class providing utilities to setup an elastica problem
  class Elastica_Basic
  {
  public:
    //! Constructor
    //! \param[in] coords Nodal coordinates (1D). Referred to, not copied
    //! \param[in] conn Element connectuivities. Referred to, not copied
    //! \param[in] ei Modulus. Copied
    //! \warning Assumes that the global coordinates have been set
    Elastica_Basic(const std::vector<double>& coords,
		   const std::vector<int>& conn,
		   const double ei);

    //! Destructor
    virtual ~Elastica_Basic();

    //! Disable copy and assignment
    Elastica_Basic(const Elastica_Basic&) = delete;
    Elastica_Basic& operator=(const Elastica_Basic&) = delete;

    //! Returns the element array
    const std::vector<Element*>& GetElementArray() const;

    //! Returns the local to global map
    const LocalToGlobalMap& GetLocalToGlobalMap() const;
    
        //! Returns the coordinates array
    const std::vector<double>& GetCoordinates() const;

    //! Returns the connectivity
    const std::vector<int>& GetConnectivity() const;

    //! Returns the number of elements
    int GetNumElements() const;

    //! Returns the number of nodes
    int GetNumNodes() const;

    //! Returns the state
    const ScalarMap& GetStateField() const;

    //! Returns the Cartesian coordinates of the nodes
    const double* GetCartesianCoordinates() const;

    //! Set the state field
    //! Useful when setting the initial guess
    void SetStateField(const double* vals);

  protected:
    //! Compute nodal cartesian coordinates
    void ComputeCartesianCoordinates(const int nodenum, const double* origin);

    // Members
    const std::vector<double>& coordinates; //!< Reference to nodal coordinates
    const std::vector<int>& connectivity; //!< Reference to element connectivities
    const double EI;
    const int nNodes; //!< Number of nodes
    const int nElements; //!< Number of elements
    std::vector<Element*> ElmArray; //!< Element array
    LocalToGlobalMap* L2GMap; //!< Local to global map

    // For state calculations
    std::vector<ElasticaStateOp*> state_OpsArray; //!< Operations to compute the state 
    StandardAssembler<ElasticaStateOp>* state_Asm; //!< Assembler for OpsArray calculations
    ScalarMap* theta; //!< Theta field
    std::vector<double> xy; //! Nodal cartesian coordinates
    
    // Monitor whether state is clean/dirty
    mutable bool state_is_dirty;
  };
}

#endif
