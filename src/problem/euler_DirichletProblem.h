// Sriramajayam

#ifndef EULER_ELASTICA_PROBLEM_H
#define EULER_ELASTICA_PROBLEM_H

#include <euler_Elastica_Basic.h>
#include <SNES_Solver.h>
#include <map>
#include <euler_Structs.h>
#include <euler_DirichletProblem_SNESFuncs.h>

namespace euler
{
  //! Class for computing solutions with Dirichlet BCs
  class DirichletProblem: public Elastica_Basic
  {
  public:
    //! Constructor
    //! \param[in] coords Nodal coordinates (1D). Referred to, not copied
    //! \param[in] conn Element connectuivities. Referred to, not copied
    //! \param[in] ei Modulus. Copied
    //! \param[in] origin Fixes the cartesian coordinates of a node
    //! \warning Assumes that the global coordinates have been set
    DirichletProblem(const std::vector<double>& coords,
		     const std::vector<int>& conn,
		     const double ei,
		     const std::pair<int,std::array<double,2>>& datuum);
    
    //! Destructor
    virtual ~DirichletProblem();

    //! Disable copy and assignment
    DirichletProblem(const DirichletProblem&) = delete;
    DirichletProblem& operator=(const DirichletProblem&) = delete;

    //! Main functionality
    //! Computes the solution theta for a given set of loads
    //! \param[in] loads Set of loads
    //! \param[in] bc_params Dirichlet BCs
    //! \param[in] solve_params Parameters for the solution algorithm
    virtual void ComputeState(const std::map<int,double>& dirichlet_bcs,
			      const SolveParams& solve_params);    
    
    //! Consistency test for state calculations
    void StateConsistencyTest(const double* stateVals,
			      const std::map<int,double>& dirichlet_bcs);

  protected:

    // Set the origin
    std::pair<int,std::array<double,2>> origin;
	
    // Members
    SNESSolver theta_solver; //!< Nonlinear solver for the state


  private:
    DirichletProblemContext ctx; //!< Context for interfacing with the SNES solver

  };
}

#endif

