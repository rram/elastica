// Sriramajayam

#ifndef EULER_DIRICHLET_PROBLEM_SNES_FUNCS_H
#define EULER_DIRICHLET_PROBLEM_SNES_FUNCS_H

#include <iostream>
#include <cassert>

namespace euler
{
  // Helper struct exposing the internals of an elastica object for assembly/solve
  struct DirichletProblemContext
  {
    StandardAssembler<ElasticaStateOp>* state_Asm; //!< Assembler
    ScalarMap* theta; //!< Fields
    Vec* tempVec; //!< Temporary for residual assembler
    const std::map<int,double>* dirichlet_bcs; //!< Dirichlet bcs
      
    //! Sanity checks
    inline void Check() const
    {
      assert(state_Asm!=nullptr && theta!=nullptr &&
	     tempVec!=nullptr && dirichlet_bcs!=nullptr);
      
      int vecSize = 0;
      VecGetSize(*tempVec, &vecSize);
      assert(vecSize>0);
  
      assert(dirichlet_bcs->size()>0);
      for(auto& it:*dirichlet_bcs)
	{ assert(it.first>=0 && it.second<vecSize); }
  
      return;
    }
  };
    
  // Assemble the residual for the state
  inline PetscErrorCode DirichletProblem_Residual_Func(SNES snes, Vec solVec, Vec resVec, void* usr)
  {
    // Get the context
    DirichletProblemContext* params;
    PetscErrorCode ierr = SNESGetApplicationContext(snes, &params); CHKERRQ(ierr);
    assert(params!=nullptr && "euler::DirichletProblem_Residual_Func- context is null");
    auto& ctx = *(params);

    // Sanity check
    ctx.Check();

    // Unpackage
    auto& theta = *ctx.theta;
    auto& Asm = *ctx.state_Asm;
    auto& dirichlet_bcs = *ctx.dirichlet_bcs;
      
    // Set the configuration at which to evaluate the residual (PETSc guess)
    int nDofs;
    ierr = VecGetSize(solVec, &nDofs); CHKERRQ(ierr);
    for(int n=0; n<nDofs; ++n)
      { double val;
	ierr = VecGetValues(solVec, 1, &n, &val); CHKERRQ(ierr);
	theta.Set(n, &val); }
    theta.SetInitialized();
      
    // Assemble the residual
    Asm.Assemble(&theta, resVec);
    ierr = VecAssemblyBegin(resVec); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(resVec); CHKERRQ(ierr);
      
    // Set dirichlet BCs into the residual
    for(auto& it:dirichlet_bcs)
      {
	const int& node = it.first;
	const double& target = it.second;
	double val = theta.Get(node)[0]-target;
	ierr = VecSetValues(resVec, 1, &node, &val, INSERT_VALUES); CHKERRQ(ierr);
      }
    ierr = VecAssemblyBegin(resVec); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(resVec); CHKERRQ(ierr);
      
    // -- done --
    return 0;
  }


  // Function for evaluating the Jacobian for the state
  inline PetscErrorCode DirichletProblem_Jacobian_Func(SNES snes, Vec solVec, Mat kMat, Mat pMat, void* usr)
  {
    // Get the context
    DirichletProblemContext* params;
    PetscErrorCode ierr = SNESGetApplicationContext(snes, &params); CHKERRQ(ierr);
    assert(params!=nullptr && "euler::DirichletProblem_Jacobian_Func- context is null");
    auto& ctx = *(params);
    
    // Sanity check
    ctx.Check();

    // Unpackage
    auto& theta = *ctx.theta;
    auto& Asm = *ctx.state_Asm;
    auto& dirichlet_bcs = *ctx.dirichlet_bcs;
    auto& tempVec = *ctx.tempVec;
      	
    // Set the configuration at which to evaluate the residual (PETSc guess)
    int nDofs;
    ierr = VecGetSize(solVec, &nDofs); CHKERRQ(ierr);
    for(int n=0; n<nDofs; ++n)
      { double val;
	ierr = VecGetValues(solVec, 1, &n, &val); CHKERRQ(ierr);
	theta.Set(n, &val); }
    theta.SetInitialized();

    // Assemble
    Asm.Assemble(&theta, tempVec, kMat);
      
    // Zero out Dirichlet dof
    for(auto& it:dirichlet_bcs)
      { ierr = MatZeroRows(kMat, 1, &it.first, 1., PETSC_NULL, PETSC_NULL); CHKERRQ(ierr); }
    ierr = MatAssemblyBegin(kMat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(kMat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    
    // -- done --
    return 0;
  }

}


#endif
