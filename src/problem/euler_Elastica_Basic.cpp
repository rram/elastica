// Sriramajayam

#include <euler_Elastica_Basic.h>
#include <P11DElement.h>
#include <iostream>
#include <cassert>

namespace euler
{
  // Constructor
  Elastica_Basic::Elastica_Basic(const std::vector<double>& coords,
				 const std::vector<int>& conn,
				 const double ei)
    :coordinates(coords),
     connectivity(conn),
     EI(ei),
     nNodes(static_cast<int>(coords.size())),
     nElements(static_cast<int>(conn.size()/2)),
     xy(2*coords.size()),
     state_is_dirty(true)
  {
    // Check that the loading nodes are in sequence
    assert(nElements==nNodes-1 && "euler::Elastica_Basic- Unexpected number of nodes/elements");
    
    // Create elements
    ElmArray.resize(nElements);
    for(int e=0; e<nElements; ++e)
      ElmArray[e] = new P11DElement<1>(connectivity[2*e], connectivity[2*e+1]);

    // Local to global map
    L2GMap = new StandardP11DMap(ElmArray);

    // State variable calculations
    //------------------------------
    // Operations: set a trivial load
    state_OpsArray.resize(nElements);
    for(int e=0; e<nElements; ++e)
      {
	ScalarMapAccess sma(std::vector<int>({connectivity[2*e]-1, connectivity[2*e+1]-1}));
	state_OpsArray[e] = new ElasticaStateOp(ElmArray[e], sma, 0, EI, 0., 0.);
      }
    // Assembler
    state_Asm = new StandardAssembler<ElasticaStateOp>(state_OpsArray, *L2GMap);
    // State
    theta = new ScalarMap(nNodes);
    double zero = 0.;
    for(int n=0; n<nNodes; ++n)
      theta->Set(n, &zero);
    theta->SetInitialized();
    
    // -- done --
  }


  // Destructor
  Elastica_Basic::~Elastica_Basic()
  {
    for(auto& x:ElmArray) delete x;
    delete L2GMap;
    for(auto& x:state_OpsArray) delete x;
    delete state_Asm;
    delete theta;
  }


  // // Returns the element array
  const std::vector<Element*>& Elastica_Basic::GetElementArray() const
  { return ElmArray; }
  
  // Returns the local to global map
  const LocalToGlobalMap& Elastica_Basic::GetLocalToGlobalMap() const
  { return *L2GMap; }
  
  // Returns the coordinates array
  const std::vector<double>& Elastica_Basic::GetCoordinates() const
  { return coordinates; }

  // Returns the connectivity
  const std::vector<int>& Elastica_Basic::GetConnectivity() const
  { return connectivity; }

  // Returns the number of elements
  int Elastica_Basic::GetNumElements() const
  { return nElements; }

  // Returns the number of nodes
  int Elastica_Basic::GetNumNodes() const
  { return nNodes; }

  // Returns the state
  const ScalarMap& Elastica_Basic::GetStateField() const
  { assert(!state_is_dirty && "euler::Elastica_Basic::GetStateField- State is dirty");
    return *theta; }

  // Returns the Cartesian coordinates of the nodes
  const double* Elastica_Basic::GetCartesianCoordinates() const
  { assert(!state_is_dirty && "euler::Elastica_Basic::GetCartesianCoordinates- state is dirty");
    return &xy[0]; }
  
  // Set the state field
  void Elastica_Basic::SetStateField(const double* vals)
  {
    const int nDof = L2GMap->GetTotalNumDof();
    for(int a=0; a<nDof; ++a)
      theta->Set(a, &vals[a]);
    theta->SetInitialized();
    state_is_dirty = true;
  }

  // Compute nodal cartesian coordinates
  void Elastica_Basic::ComputeCartesianCoordinates(const int nodenum,
						   const double* origin)
  {
    assert(state_is_dirty==false);
    
    std::fill(xy.begin(), xy.end(), 0.);
    xy[0] = 0.;
    xy[1] = 0.;
    const int nElements = static_cast<int>(ElmArray.size());
    const int nNodes = L2GMap->GetTotalNumDof();
    const auto& dofvals = theta->Get();
    double delta_x, delta_y, thetaval;
    for(int e=0; e<nElements; ++e)
      {
	const auto* Elm = ElmArray[e];
	const auto& Qwts = Elm->GetIntegrationWeights(0);
	const int nQuad = static_cast<int>(Qwts.size());
	const int nDof = Elm->GetDof(0);
	delta_x = 0.; // integral over this element
	delta_y = 0.;
	for(int q=0; q<nQuad; ++q)
	  {
	    // theta value here
	    thetaval = 0.;
	    for(int a=0; a<nDof; ++a)
	      thetaval += dofvals[L2GMap->Map(0,a,e)]*Elm->GetShape(0,q,a);

	    // Integrate
	    delta_x += Qwts[q]*std::cos(thetaval);
	    delta_y += Qwts[q]*std::sin(thetaval);
	  }

	// Update Cartesian coordinates
	xy[2*(e+1)+0] = xy[2*e+0] + delta_x;
	xy[2*(e+1)+1] = xy[2*e+1] + delta_y;
      }

    // Set the given coordinates at the specified node number
    // ie, translate coordinates
    double tvec[] = {origin[0]-xy[2*nodenum], origin[1]-xy[2*nodenum+1]};
    for(int a=0; a<nNodes; ++a)
      {
	xy[2*a] += tvec[0];
	xy[2*a+1] += tvec[1];
      }
    assert(std::abs(xy[2*nodenum]-origin[0])<1.e-8);
    assert(std::abs(xy[2*nodenum+1]-origin[1])<1.e-8);

    // done
    return;
  }

}
