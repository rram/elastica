// Sriramajayam

#ifndef RBC_ELASTICA_CONTEXT_H
#define RBC_ELASTICA_CONTEXT_H

#include <cassert>
#include <vector>
#include <array>
#include <petscvec.h>
#include <Element.h>
#include <LocalToGlobalMap.h>
#include <rbc_Structs.h>

namespace rbc
{
  // Forward declarations
  class ElasticaStateOp;
  class ElasticaHSensitivityOp;
  class ElasticaVSensitivityOp;
  class ElasticaBCSensitivityOp;
  class Elastica;
  
  namespace detail
  {
    // Helper struct exposing the internals of an elastica object for assembly/solve
    struct ElasticaContext
    {
      Elastica* elastica;
      const std::vector<Element*>* ElmArray; //!< Element array
      const LocalToGlobalMap* L2GMap; //!< Local to global map

      // Operations
      std::vector<ElasticaStateOp*>* state_OpsArray;
      std::vector<ElasticaHSensitivityOp*>* HSense_OpsArray;
      std::vector<ElasticaVSensitivityOp*>* VSense_OpsArray;
      std::vector<ElasticaBCSensitivityOp*>* BCSense_OpsArray;

      // Assemblers
      StandardAssembler<ElasticaStateOp>* state_Asm;
      StandardAssembler<ElasticaHSensitivityOp>* HSense_Asm;
      StandardAssembler<ElasticaVSensitivityOp>* VSense_Asm;
      StandardAssembler<ElasticaBCSensitivityOp>* BCSense_Asm; 

      // Fields
      ScalarMap* theta;
      ScalarMap* alpha_H;  
      ScalarMap* alpha_V;  
      ScalarMap* alpha_BC;
      ScalarMap* alpha_M;

      // Loads/BCs
      const LoadParams* load_params;
      const DirichletBCs* bc_params;
      const SolveParams* solve_params;

      // Sensitivity type
      SensitivityType stype;
      
      // Temporary vector for residual assembly
      Vec* tempVec;

      // Sanity check
      inline void Check() const
      {
	assert(elastica!=nullptr &&
	       ElmArray!=nullptr && L2GMap!=nullptr &&
	       state_OpsArray!=nullptr && HSense_OpsArray!=nullptr &&
	       VSense_OpsArray!=nullptr && BCSense_OpsArray!=nullptr &&
	       state_Asm!=nullptr && HSense_Asm!=nullptr &&
	       VSense_Asm!=nullptr && BCSense_Asm!=nullptr &&
	       theta!=nullptr && alpha_H!=nullptr && alpha_V!=nullptr &&
	       alpha_BC!=nullptr && alpha_M!=nullptr && tempVec!=nullptr &&
	       load_params!=nullptr && bc_params!=nullptr && solve_params!=nullptr);

	const int nElm = L2GMap->GetNumElements();
	assert(nElm==static_cast<int>(ElmArray->size()) &&
	       nElm==static_cast<int>(state_OpsArray->size()) &&
	       nElm==static_cast<int>(HSense_OpsArray->size()) &&
	       nElm==static_cast<int>(VSense_OpsArray->size()) &&
	       nElm==static_cast<int>(BCSense_OpsArray->size()));
	
	const int nDofs = L2GMap->GetTotalNumDof();
	assert(theta->GetNumNodes()==nDofs);
	assert(alpha_H->GetNumNodes()==nDofs);
	assert(alpha_V->GetNumNodes()==nDofs);
	assert(alpha_BC->GetNumNodes()==nDofs);
	assert(alpha_M->GetNumNodes()==nDofs);
	solve_params->Check();

	load_params->Check();
	bc_params->Check();
	solve_params->Check();

	// Dirichlet and moment dofs have to be different
	assert(load_params->MDof!=bc_params->dof);

	return;
      }
      
    }; // end of class
     
  } // end of namespace
  
}


#endif
