// Sriramajayam

#include <euler_DirichletProblem.h>
#include <P11DElement.h>
#include <random>
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a uniform 1D mesh on the interval [0,1]
  // ------------------------------------------------
  const int nNodes = 10;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  std::cout<<"\nNumber of elements: "<<nElements<<std::flush;

  // Set the 
  std::pair<int,std::array<double,2>> origin;
  origin.first = 0;
  origin.second = std::array<double,2>{0.,0.};
  
  // Create the dirichlet problem
  const double EI = 1.;
  euler::DirichletProblem* str = new euler::DirichletProblem(coordinates, connectivity, EI, origin);
  
  // Dirichlet BCs: clamp the left end, rotate the right end
  std::map<int,double> dirichlet_bcs{};
  dirichlet_bcs[0] = 0.;
  dirichlet_bcs[nNodes-1] = 3.14159;
  
  // Solve parameters
  euler::SolveParams solve_params{.EPS = 1.e-10, .resScale = 1.,
      .dofScale = 1.,	.nMaxIters = 25, .verbose = true};
  
  // Compute the state 
  str->ComputeState(dirichlet_bcs, solve_params);

  // Plot
  const auto& state = str->GetStateField().Get();
  const double* xy = str->GetCartesianCoordinates();
  std::fstream stream;
  stream.open("ss.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    stream << coordinates[n] << " "<<state[n]<<" "<<xy[2*n]<<" "<<xy[2*n+1]<<"\n";
  stream.close();

  exit(1);
  // Clean up
  delete str;
  PetscFinalize();
}
