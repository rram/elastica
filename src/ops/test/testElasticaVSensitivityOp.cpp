// Sriramajayam

#include <euler_ElasticaVSensitivityOp.h>
#include <P11DElement.h>
#include <euler_Structs.h>
#include <random>
#include <cassert>
#include <cmath>

using namespace euler;

int main()
{
  // Create one segment
  std::vector<double> coordinates({std::sqrt(2.), std::sqrt(10.)});
  Segment<1>::SetGlobalCoordinatesArray(coordinates);
  P11DElement<1> Elm({1,2});

  // Random number generator
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> dist(-1.,1.);

  // State a random state
  const int nNodes = 2;
  ScalarMap state(nNodes);
  double val;
  for(int a=0; a<2; ++a)
    {
      val = dist(gen);
      state.Set(a, &val);
    }
  state.SetInitialized();

  // Set a random sensitivity field
  ScalarMap sensitivity(nNodes);
  for(int a=0; a<2; ++a)
    {
      val = dist(gen);
      sensitivity.Set(a, &val);
    }
  sensitivity.SetInitialized();

  // Create a configuration
  Sensitivity_and_State config = std::make_pair(&sensitivity, &state);
    
  // Access to scalar map
  ScalarMapAccess SMA(std::vector<int>({0,1}));

  // Tolerances
  const double pertEPS = 1.e-5;
  const double tolEPS = 1.e-5;
  
  // Test class
  const double EI = std::sqrt(10.)+dist(gen);
  const double Lambda[] = {std::sqrt(5.)+dist(gen), std::sqrt(5.)-dist(gen)};
  ElasticaVSensitivityOp Op(&Elm, SMA, 0, EI, Lambda[0], Lambda[1]);
  assert(Op.GetField().size()==1 && "Unexpected number of fields");
  assert(Op.GetField()[0]==0 && "Unexpected field number");
  assert(Op.GetFieldDof(0)==2 && "Unexpected number of dofs");
  assert(Op.GetElement()==&Elm && "Unexpected element returned");
  assert(Op.ConsistencyTest(&config, pertEPS, tolEPS) && "Failed consistency test");
  assert(std::abs(Op.GetModulus()-EI)<1.e-10);
  double lambda[2];
  Op.GetLoadValues(lambda[0], lambda[1]);
  assert(std::abs(lambda[0]-Lambda[0])+std::abs(lambda[1]-Lambda[1]) < 1.e-10);
  
  // Copy
  ElasticaVSensitivityOp Copy(Op);
  assert(Copy.GetField()==Op.GetField() && "Unexpected fields in copy");
  assert(Copy.GetFieldDof(0)==Op.GetFieldDof(0) && "Unexpected number of dofs in copy");
  assert(Copy.GetElement()==Op.GetElement() && "Unexpected element returned in copy");
  assert(Copy.ConsistencyTest(&config, pertEPS, tolEPS) && "Failed consistency test in copy");
  assert(std::abs(Copy.GetModulus()-EI)<1.e-10);
  Copy.GetLoadValues(lambda[0], lambda[1]);
  assert(std::abs(lambda[0]-Lambda[0])+std::abs(lambda[1]-Lambda[1]) < 1.e-10);
  

  // Cloning
  auto* Clone = Copy.Clone();
  assert(Clone->GetField()==Op.GetField() && "Unexpected fields in clone.");
  assert(Clone->GetFieldDof(0)==Op.GetFieldDof(0) && "Unexpected number of dofs in clone.");
  assert(Clone->GetElement()==Op.GetElement() && "Unexpected element returned in clone.");
  assert(Clone->ConsistencyTest(&config, pertEPS, tolEPS) && "Failed consistency test in clone.");
  assert(std::abs(Clone->GetModulus()-EI)<1.e-10);
  Clone->GetLoadValues(lambda[0], lambda[1]);
  assert(std::abs(lambda[0]-Lambda[0])+std::abs(lambda[1]-Lambda[1]) < 1.e-10);
  
  delete Clone;
}
