// Sriramajayam

#include <euler_ElasticaStateOp.h>
#include <cassert>
#include <cmath>

namespace euler
{
  // Compute the bending eenrgy at a given state
  double ElasticaStateOp::GetEnergy(const void* arg) const
  {
    // Get this state
    assert(arg!=nullptr && "euler::ElasticaStateOp::GetEnergy- Invalid pointer");
    const ScalarMap* state = static_cast<const ScalarMap*>(arg);
    assert(state!=nullptr && "euler::ElasticaStateOp::GetEnergy- Invalid pointer");
    assert(state->IsInitialized() && "euler::ElasticaStateOp::GetEnergy: Dofs not initialized");
  
    // Local node numbers to access scalar field
    const auto& LocNodes = SMAccess.Get();

    // Fields, ndofs, shape function derivatives
    const int field = Fields[0];
    const int nDof = GetFieldDof(0);
    const int nDeriv = Elm->GetNumDerivatives(field);
    assert(nDeriv==1 && "euler::ElasticaStateOp::GetEnergy- unexpected number of derivatives");
    const auto& Shapes = Elm->GetShape(field);
    const auto& DShapes = Elm->GetDShape(field);

    // Quadrature
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());

    // Integrate
    double Energy = 0.;
    double theta = 0.; 
    double dtheta = 0.; //  = theta'
    for(int q=0; q<nQuad; ++q)
      {
	// Shape functions and derivatives  at this quadrature point
	const double* qShapes = &Shapes[q*nDof];
	const double* qDShapes = &DShapes[q*nDof];
	theta = 0.;
	dtheta = 0.;
	for(int a=0; a<nDof; ++a)
	  {
	    theta += state->Get(LocNodes[a])[0]*qShapes[a];
	    dtheta += state->Get(LocNodes[a])[0]*qDShapes[a];
	  }

	// Update energy
	Energy += Qwts[q]*(0.5*EI*dtheta*dtheta -
			   LambdaH*std::cos(theta) -
			   LambdaV*std::sin(theta));
      }
    return Energy;
  }


  // Residual and stiffness calculation
  void ElasticaStateOp::GetDVal(const void *arg, 
				    std::vector<std::vector<double>>* funcval, 
				    std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
  {
    // get this state
    assert(arg!=nullptr && "euler::ElasticaStateOp::GetDVal- invalid pointer for configuration");
    const ScalarMap* state = static_cast<const ScalarMap*>(arg);
    assert(state!=nullptr && "euler::ElasticaStateOp::GetDVal- invalid pointer for configuration");
    assert(state->IsInitialized() && "euler::ElasticaStateOp::GetDVal: Dofs not initialized");

    // Local dof numbers to access scalar field
    const auto& LocNodes = SMAccess.Get();

    // Zero the outputs
    SetZero(funcval, dfuncval);

    // Fields, ndofs, num of derivatives
    const int field = Fields[0];
    const int nDof = GetFieldDof(field);
    const int nDeriv = Elm->GetNumDerivatives(field);
    assert(nDeriv==1 && "euler::ElasticaStateOp::GetDVal- unexpected number of derivatives");
    const auto& Shapes = Elm->GetShape(field);
    const auto& DShapes = Elm->GetDShape(field);
  
    // Quadrature
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());

    // Integrate
    double theta;
    double dtheta;
    for(int q=0; q<nQuad; ++q)
      {
	// Shape functions and derivatices  at this quadrature point
	const double* qShapes = &Shapes[q*nDof];
	const double* qDShapes = &DShapes[q*nDof];

	// Compute theta & theta' here
	theta = 0.;
	dtheta = 0.;
	for(int a=0; a<nDof; ++a)
	  {
	    theta += state->Get(LocNodes[a])[0]*qShapes[a];
	    dtheta += state->Get(LocNodes[a])[0]*qDShapes[a]; // 1D
	  }

	// Udpate the force vector
	if(funcval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    // This variation delta F = N_a
	    (*funcval)[0][a] += Qwts[q]*(EI*dtheta*qDShapes[a] +
					 LambdaH*std::sin(theta)*qShapes[a]-
					 LambdaV*std::cos(theta)*qShapes[a]);

	// Update the stiffness
	if(dfuncval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    // This variation delta F = N_a
	    for(int b=0; b<nDof; ++b)
	      // This variation DELTA F = N_b
	      (*dfuncval)[0][a][0][b] += Qwts[q]*(EI*qDShapes[a]*qDShapes[b] +
						  LambdaH*std::cos(theta)*qShapes[a]*qShapes[b] +
						  LambdaV*std::sin(theta)*qShapes[a]*qShapes[b]);
      }
    return;
  }


  // Consistency test
  bool ElasticaStateOp::ConsistencyTest(const void* arg,
					    const double pertEPS,
					    const double tolEPS) const
  {
    assert(arg!=nullptr && "euler::ElasticaStateOp::ConsistencyTest- invalid pointer to state");
    const ScalarMap* ptr = static_cast<const ScalarMap*>(arg);
    assert(ptr!=nullptr && "euler::ElasticaStateOp::ConsistencyTest- invalid pointer to state");
    const ScalarMap& state = *ptr;
    assert(state.IsInitialized() && "euler::ElasticaStateOp::Consistency test: Dofs not initialized");
  
    // Local dof numbers to access scalar map
    const auto& LocNodes = SMAccess.Get();

    // Fields and dofs
    const int field = Fields[0];
    const int nDof = GetFieldDof(field);

    // Size arrays
    std::vector<std::vector<double>> res(1), resnum(1), resplus(1), resminus(1);
    res[0].resize(nDof);
    resnum[0].resize(nDof);
    resplus[0].resize(nDof);
    resminus[0].resize(nDof);

    std::vector<std::vector<std::vector<std::vector<double>>>> dres(1), dresnum(1);
    dres[0].resize(nDof);
    dresnum[0].resize(nDof);
    for(int a=0; a<nDof; ++a)
      {
	dres[0][a].resize(1);
	dres[0][a][0].resize(nDof);
	dresnum[0][a].resize(1);
	dresnum[0][a][0].resize(nDof);
      }

    // Implemented residual and stiffness
    GetDVal(arg, &res, &dres);

    // Consistency tests
    const double mpertEPS = -pertEPS;
    for(int a=0; a<nDof; ++a)
      {
	// Positive and negative perturbations
	ScalarMap pstate(state);
	pstate.Increment(LocNodes[a], &pertEPS);
	pstate.SetInitialized();
	ScalarMap mstate(state);
	mstate.Increment(LocNodes[a], &mpertEPS);
	mstate.SetInitialized();

	// Compute perturbed energies
	double Eplus = GetEnergy(&pstate);
	double Eminus = GetEnergy(&mstate);
	double nres = (Eplus-Eminus)/(2.*pertEPS);
	assert(std::abs(nres-res[0][a])<tolEPS && "euler::ElasticaStateOp::Consistency of residuals failed");
	
	// Compute perturbed residuals
	GetVal(&pstate, &resplus);
	GetVal(&mstate, &resminus);

	for(int b=0; b<nDof; ++b)
	  {
	    double ndres = (resplus[0][b]-resminus[0][b])/(2.*pertEPS);
	    assert(std::abs(ndres-dres[0][a][0][b])<tolEPS && "euler::ElasticaStateOp::Consistency of residuals failed");
	  }
      }
    return true;
  }

}
