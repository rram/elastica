// Sriramajayam

#include <euler_ElasticaBCSensitivityOp.h>
#include <euler_Structs.h>
#include <cassert>
#include <cmath>
#include <iostream>

namespace euler
{
  // Compute the energy functional
  double ElasticaBCSensitivityOp::GetEnergy(const void* arg) const
  {
    // Get this state
    assert(arg!=nullptr && "euler::ElasticaBCSensitivityOp::GetEnergy- Invalid pointer");
    const Sensitivity_and_State* config = static_cast<const Sensitivity_and_State*>(arg);
    assert(config!=nullptr && "euler::ElasticaBCSensitivityOp::GetEnergy- Invalid pointer");
    const SensitivityField* sensitivity = config->first;
    const StateField* state = config->second;
    assert((sensitivity!=nullptr && state!=nullptr) &&
	   "euler::ElasticaBCSensitivityOp::GetEnergy- State and/or sensitivity pointers are null");
    assert((sensitivity->IsInitialized() && state->IsInitialized()) &&
	   "euler::ElasticaBCSensitivityOp::GetEnergy: State and/or sensitivity not initialized");
  
    // Local node numbers to access scalar field
    const auto& LocNodes = SMAccess.Get();

    // Fields, ndofs, shape function derivatives
    const int field = Fields[0];
    const int nDof = GetFieldDof(0);
    const int nDeriv = Elm->GetNumDerivatives(field);
    assert(nDeriv==1 && "euler::ElasticaBCSensitivityOp::GetEnergy- unexpected number of derivatives");
    const auto& Shapes = Elm->GetShape(field);
    const auto& DShapes = Elm->GetDShape(field);

    // Quadrature
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());

    // Integrate
    double Energy = 0.;
    double theta; 
    double alpha;
    double dalpha;
    for(int q=0; q<nQuad; ++q)
      {
	// Shape functions and derivatives  at this quadrature point
	const double* qShapes = &Shapes[q*nDof];
	const double* qDShapes = &DShapes[q*nDof];
	theta = 0.;
	alpha = 0.;
	dalpha = 0.;
	for(int a=0; a<nDof; ++a)
	  {
	    theta += state->Get(LocNodes[a])[0]*qShapes[a];
	    alpha += sensitivity->Get(LocNodes[a])[0]*qShapes[a];
	    dalpha += sensitivity->Get(LocNodes[a])[0]*qDShapes[a];
	  }

	// Update energy
	Energy += Qwts[q]*( 0.5*EI*dalpha*dalpha +
			    0.5*alpha*alpha*(LambdaH*std::cos(theta) + LambdaV*std::sin(theta)) );
      }
    return Energy;
  }


  // Residual and stiffness calculation
  void ElasticaBCSensitivityOp::GetDVal(const void *arg, 
				     std::vector<std::vector<double>>* funcval, 
				     std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
  {
    // get this state
    assert(arg!=nullptr && "euler::ElasticaBCSensitivityOp::GetDVal- invalid pointer for configuration");
    const Sensitivity_and_State* config = static_cast<const Sensitivity_and_State*>(arg);
    assert(config!=nullptr && "euler::ElasticaBCSensitivityOp::GetDVal- invalid pointer for configuration");
    const SensitivityField* sensitivity = config->first;
    const StateField* state = config->second;
    assert((sensitivity!=nullptr && state!=nullptr) &&
	   "euler::ElasticaBCSensitivityOp::GetDVal- Sensitivity and/or state are null");
    assert((sensitivity->IsInitialized() && state->IsInitialized()) &&
	   "euler::ElasticaBCSensitivityOp::GetDVal- Sensitivity and/or state are not initialized");
    
    // Local dof numbers to access scalar field
    const auto& LocNodes = SMAccess.Get();

    // Zero the outputs
    SetZero(funcval, dfuncval);
    
    // Fields, ndofs, num of derivatives
    const int field = Fields[0];
    const int nDof = GetFieldDof(field);
    const int nDeriv = Elm->GetNumDerivatives(field);
    assert(nDeriv==1 && "euler::ElasticaBCSensitivityOp::GetDVal- unexpected number of derivatives");
    const auto& Shapes = Elm->GetShape(field);
    const auto& DShapes = Elm->GetDShape(field);
  
    // Quadrature
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());

    // Integrate
    double theta;
    double alpha;
    double dalpha;
    for(int q=0; q<nQuad; ++q)
      {
	// Shape functions and derivatices  at this quadrature point
	const double* qShapes = &Shapes[q*nDof];
	const double* qDShapes = &DShapes[q*nDof];

	// Compute theta, alpha & alpha' here
	theta = 0.;
	alpha = 0.;
	dalpha = 0.;
	for(int a=0; a<nDof; ++a)
	  {
	    theta += state->Get(LocNodes[a])[0]*qShapes[a];
	    alpha += sensitivity->Get(LocNodes[a])[0]*qShapes[a];
	    dalpha += sensitivity->Get(LocNodes[a])[0]*qDShapes[a]; // 1D
	  }

	// Udpate the force vector
	if(funcval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    // This variation delta F = N_a
	    (*funcval)[0][a] += Qwts[q]*(EI*dalpha*qDShapes[a] +
					 LambdaH*std::cos(theta)*alpha*qShapes[a]+
					 LambdaV*std::sin(theta)*alpha*qShapes[a]);
	
	// Update the stiffness
	if(dfuncval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    // This variation delta F = N_a
	    for(int b=0; b<nDof; ++b)
	      // This variation DELTA F = N_b
	      (*dfuncval)[0][a][0][b] += Qwts[q]*(EI*qDShapes[a]*qDShapes[b] +
						  LambdaH*std::cos(theta)*qShapes[a]*qShapes[b]+
						  LambdaV*std::sin(theta)*qShapes[a]*qShapes[b]);
      }
    return;
  }
  
  
  // Consistency test
  bool ElasticaBCSensitivityOp::ConsistencyTest(const void* arg,
					     const double pertEPS,
					     const double tolEPS) const
  {
    assert(arg!=nullptr && "euler::ElasticaBCSensitivityOp::ConsistencyTest- invalid pointer to state");
    const Sensitivity_and_State* ptr = static_cast<const Sensitivity_and_State*>(arg);
    assert(ptr!=nullptr && "euler::ElasticaBCSensitivityOp::ConsistencyTest- invalid pointer to sensitivity/state");
    const SensitivityField& sensitivity = *ptr->first;
    const StateField& state = *ptr->second;
    assert((state.IsInitialized() && sensitivity.IsInitialized()) &&
	   "euler::ElasticaBCSensitivityOp::Consistency test: State/Sensitivity not initialized");
  
    // Local dof numbers to access scalar map
    const auto& LocNodes = SMAccess.Get();

    // Fields and dofs
    const int field = Fields[0];
    const int nDof = GetFieldDof(field);

    // Size arrays
    std::vector<std::vector<double>> res(1), resnum(1), resplus(1), resminus(1);
    res[0].resize(nDof);
    resnum[0].resize(nDof);
    resplus[0].resize(nDof);
    resminus[0].resize(nDof);

    std::vector<std::vector<std::vector<std::vector<double>>>> dres(1), dresnum(1);
    dres[0].resize(nDof);
    dresnum[0].resize(nDof);
    for(int a=0; a<nDof; ++a)
      {
	dres[0][a].resize(1);
	dres[0][a][0].resize(nDof);
	dresnum[0][a].resize(1);
	dresnum[0][a][0].resize(nDof);
      }

    // Implemented residual and stiffness
    GetDVal(arg, &res, &dres);

    // Consistency tests
    const double mpertEPS = -pertEPS;
    for(int a=0; a<nDof; ++a)
      {
	// Positive and negative perturbations
	SensitivityField psens(sensitivity);
	psens.Increment(LocNodes[a], &pertEPS);
	psens.SetInitialized();
	Sensitivity_and_State pconfig = std::make_pair(&psens, &state);
	
	SensitivityField msens(sensitivity);
	msens.Increment(LocNodes[a], &mpertEPS);
	msens.SetInitialized();
	Sensitivity_and_State mconfig = std::make_pair(&msens, &state);

	// Compute perturbed energies
	double Eplus = GetEnergy(&pconfig);
	double Eminus = GetEnergy(&mconfig);
	double nres = (Eplus-Eminus)/(2.*pertEPS);
	//std::cout<<"\n"<<res[0][a]<<" should be "<<nres<<std::flush;
	assert(std::abs(nres-res[0][a])<tolEPS && "euler::ElasticaBCSensitivityOp::Consistency of residuals failed");
	
	// Compute perturbed residuals
	GetVal(&pconfig, &resplus);
	GetVal(&mconfig, &resminus);

	for(int b=0; b<nDof; ++b)
	  {
	    double ndres = (resplus[0][b]-resminus[0][b])/(2.*pertEPS);
	    //std::cout<<"\n"<<dres[0][a][0][b]<<" should be "<<ndres<<std::flush;
	    assert(std::abs(ndres-dres[0][a][0][b])<tolEPS && "euler::ElasticaBCSensitivityOp::Consistency of residuals failed");
	  }
      }
    return true;
  }
  
}
