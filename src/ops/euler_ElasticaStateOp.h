// Sriramajayam

#ifndef EULER_ELASTICA_STATE_H
#define EULER_ELASTICA_STATE_H

#include <ElementalOperation.h>
#include <euler_ScalarMap.h>

namespace euler
{
  class ElasticaStateOp: public DResidue
  {
  public:
    //! Constructor
    //! \param[in] elm Element for this operation. Same interpolation for all fields
    //! \param[in] fieldnum Field number for dof angle
    //! \param[in] str Configuration access
    inline ElasticaStateOp(const Element* elm,
			   ScalarMapAccess sma, const int field,
			   const double ei,
			   const double lambdaH, const double lambdaV)
      :DResidue(), Elm(elm),  SMAccess(sma),  Fields({field}), EI(ei),
      LambdaH(lambdaH), LambdaV(lambdaV) {}
    
    
    //! Destructor. Nothing to do
    inline virtual ~ElasticaStateOp() {}
    
    //! Copy constructor
    //! \param[in] Obj Object to be copied
    inline ElasticaStateOp(const ElasticaStateOp& Obj)
      :DResidue(Obj),
      Elm(Obj.Elm),
      SMAccess(Obj.SMAccess),
      Fields(Obj.Fields),
      EI(Obj.EI),
      LambdaH(Obj.LambdaH),
      LambdaV(Obj.LambdaV)
	{}

    //! Cloning
    inline virtual ElasticaStateOp* Clone() const override
    { return new ElasticaStateOp(*this); }
    
    //! Disable assignment
    ElasticaStateOp& operator=(const ElasticaStateOp&) = delete;

    //! Returns the element being used
    inline const Element* GetElement() const
    { return Elm; }

    //! Returns the fields used
    inline const std::vector<int>& GetField() const override
    { return Fields; }

    //! Returns the number of dofs for a given field
    inline int GetFieldDof(int fieldnumber) const override
    { return Elm->GetDof(Fields[fieldnumber]); }

    //! Returns the modulus
    inline double GetModulus() const
    { return EI; }

    //! Returns the load
    inline void GetLoads(double& hval, double& vval) const
    { hval=LambdaH; vval=LambdaV; return; }

    //! Set the load
    inline void SetLoad(const double hval, const double vval)
    { LambdaH = hval; LambdaV = vval; }

    //! Computes the energy functional
    //! \param[in] Config Configuration at which to compute the energy
    double GetEnergy(const void* Config) const;

    //! Residual calculation
    //! \param[in] Config Configuration at which to compute the residual
    //! \param[in] resval Computed local residual
    inline void GetVal(const void* Config,
		std::vector<std::vector<double>>* resval) const override
    { return GetDVal(Config, resval, nullptr); }

    //! Residual and stiffness calculation
    //! \param[in] Config Configuration at which to compute the residual
    //! \param[in] resval Computed local residual
    //! \param[in] dresval Computed local stiffness
    void GetDVal(const void* Config, 
		 std::vector<std::vector<double>>* resval,
		 std::vector<std::vector<std::vector<std::vector<double>>>>* dresval) const override;

    //! Consistency test for this operation
    //! \param[in] Config Configuration at which to perform the consistency test
    //! \param[in] pertEPS Tolerance to use for perturbations
    //! \param[in] tolEPS Tolerance to use for examining outputs
    bool ConsistencyTest(const void* Config,
				 const double pertEPS, const double tolEPS) const override;

  private:
    const Element* Elm; //!< Element for this operation
    const ScalarMapAccess SMAccess; //!< Details to access configuration for this operation
    std::vector<int> Fields; //!< Field number for this operation
    const double EI; //!< Bending modulus
    double LambdaH;  //!< Load value in the horizontal direction
    double LambdaV; //!< Load value in the vertical direction
  };
    
}


#endif
