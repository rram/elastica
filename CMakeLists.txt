# Sriramajayam

cmake_minimum_required(VERSION 3.12)

project(euler
  VERSION 1.0
  DESCRIPTION "Euler Elastica")

# Set runtime path (for Linux)
set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/${PROJECT_NAME}/lib")
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
  
# dependencies: dg++
find_package(dgpp REQUIRED)

list(APPEND euler_COMPILE_FEATURES
  "cxx_constexpr"
  "cxx_auto_type"
  "cxx_decltype"
  "cxx_decltype_auto"
  "cxx_enum_forward_declarations"
  "cxx_inline_namespaces"
  "cxx_lambdas"
  "cxx_nullptr"
  "cxx_override"
  "cxx_range_for")

# Compile code in src
add_subdirectory(src)

# Install
include(GNUInstallDirs)
include(CMakePackageConfigHelpers)

install(TARGETS
  euler_utils
  euler_ops
  euler_problem
  EXPORT ${PROJECT_NAME}Targets
  DESTINATION ${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR}
  INCLUDES DESTINATION ${PROJECT_NAME}/${CMAKE_INSTALL_INCLUDEDIR})

install(EXPORT ${PROJECT_NAME}Targets
  DESTINATION ${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
  NAMESPACE euler::
  FILE ${PROJECT_NAME}Targets.cmake)

set(EULER_INCLUDE_DIRS_VAR ${PROJECT_NAME}/${CMAKE_INSTALL_INCLUDEDIR})
set(EULER_LIBRARY_DIRS_VAR ${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR})

configure_package_config_file(
  "cmake/${PROJECT_NAME}Config.cmake.in" "${PROJECT_NAME}Config.cmake"
  INSTALL_DESTINATION
  ${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
  PATH_VARS EULER_INCLUDE_DIRS_VAR EULER_LIBRARY_DIRS_VAR)

write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake
  VERSION ${PACKAGE_VERSION}
  COMPATIBILITY AnyNewerVersion)

install(FILES
  "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
  "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake"
  DESTINATION "${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}")


include(CTest)
add_subdirectory(src/ops/test)
add_subdirectory(src/problem/test)
